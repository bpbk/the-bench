<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package The_Bench
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="stylesheet" href="https://use.typekit.net/sxw3zgz.css">
	<link href="//db.onlinewebfonts.com/c/af2adb855bbec3740bbddaf306b497fa?family=YoungSerif" rel="stylesheet" type="text/css"/>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<?php if ( is_front_page() ) { ?>
		<div class="navigation-container">
			<nav id="site-navigation" class="main-navigation">
				<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'the-bench' ); ?></button>
				<?php
				if ( $sections = get_field('sections', get_option('page_on_front')) ) {
					foreach ( $sections as $section ) {
						if ( $section['menu_title'] != '' ) { ?>
							<a href="<?php echo is_home() ? '' : home_url() . '/'; echo '#' . slugify($section['menu_title']); ?>"><?php echo $section['menu_title']; ?></a>
						<?php
						}
					}
				} ?>
			</nav><!-- #site-navigation -->
		</div>
	<?php } ?>

<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'the-bench' ); ?></a>

	<header id="masthead" class="site-header">
		<div class="site-branding">
			<a class="site-title" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.svg"/></a>
			<a class="linked-in" target="_blank" href="https://www.linkedin.com/company/the-bench-nyc/about/" alt="linkedin"><img src="<?php echo get_template_directory_uri(); ?>/images/linkedin.svg"></a>
		</div><!-- .site-branding -->
	</header><!-- #masthead -->

	<div id="content" class="site-content">
