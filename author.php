<?php get_header();

$user = get_queried_object();
$user_id = $user->ID; ?>

	<main role="main">

		<div class="navigation-container">
			<nav id="site-navigation" class="main-navigation">
				<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'the-bench' ); ?></button>
				<a href="#candidate">Candidate</a>
				<?php
				if ( get_field('experience', 'user_' . $user_id) ) { ?>
					<a href="#experience">experience</a>
				<?php
				}
				if ( get_field('resume', 'user_' . $user_id) ) { ?>
					<a href="#resume">Resume</a>
				<?php
				} ?>
			</nav><!-- #site-navigation -->
		</div>

		<!-- section -->
		<section>
      <header id="candidate-header">
        <section class="content header-padding">
					<a class="anchor-tag" name="candidate"></a>
					<h1><span><?php _e( 'Candidate ', 'html5blank' ); ?></span> #<?php echo $user_id; ?></h1>
					<?php
					if ( $current_position = get_field('current_position', 'user_' . $user_id ) ) { ?>
						<h6><span>Currently</span> <?php echo $current_position; ?></h6>
					<div class="row justify-between">
						<div class="col-5">
		          <?php
		          }
		          if ( $intro = get_field('introduction', 'user_' . $user_id ) ) { ?>
		            <div class="candidate-intro">
		              <?php echo $intro; ?>
		            </div>
		          <?php
							} ?>
						</div>
						<div class="col-7 illustration-container">
							<img src="<?php echo get_template_directory_uri(); ?>/images/illustrations/pencil.svg">
						</div>
					</div>
					<?php
					if ( $logos = get_field('job_history_logos') ) { ?>
						<div id="job-history-logos">
							<?php
							foreach ( $logos as $logo ) { ?>
								<div class="job-logo">
									<?php echo $logo['url'] ? '<a href="' . $logo['url'] . '" target="_blank">' : ''; ?>
										<img alt="<?php echo $logo['company']; ?>" src="<?php echo $logo['logo']['sizes']['small']; ?>"/>
									<?php echo $logo['url'] ? '</a>' : ''; ?>
								</div>
							<?php
							} ?>
						</div>
					<?php
					} ?>
        </section>

      </header>

      <?php
      if ( $experience = get_field('experience', 'user_' . $user_id) ) { ?>
        <section id="candidate-exp">
					<a class="anchor-tag" name="experience"></a>
          <div class="content">
            <h2><span>experience</span></h2>
            <div class="the-bench-chart">
              <?php
              $max_years = 0;
							$total_exp = get_field('total_experience', 'user_' . $user_id );
              usort($experience, 'compare_experience');
              foreach ( $experience as $exp ) {
                $max_years = $exp['years'] > $max_years ? $exp['years'] : $max_years;
              } ?>
							<div class="chart-item">
								<div class="chart-label">
									Total Design Experience
								</div>
								<div class="chart-bar-container">
									<div class="chart-bar" style="width: 100%">
										<span></span>
									</div>
								</div>
								<div class="chart-val">
									<p><strong><?php echo $total_exp; ?></strong></p>
									<p>Years</p>
								</div>
							</div>
							<?php
              foreach ( $experience as $exp ) { ?>
                <div class="chart-item">
                  <div class="chart-label">
                    <?php echo $exp['type']; ?>
                  </div>
                  <div class="chart-bar-container">
                    <div class="chart-bar" style="width: <?php echo 100 / $total_exp * $exp['years']; ?>%">
											<span></span>
                    </div>
                  </div>
                  <div class="chart-val">
                    <p><strong><?php echo $exp['years']; ?></strong></p>
                    <p>Years</p>
                  </div>
                </div>
              <?php
              } ?>
            </div>
          </div>
        </div>
      <?php
      }

      echo get_page_form( [
        'shortcode' => '[caldera_form id="CF5d285df0830d4"]',
        'color' => 'white'
      ] );

      if ( $resume = get_field('resume', 'user_' . $user_id) ) { ?>
        <section id="candidate-resume">
					<a class="anchor-tag" name="resume"></a>
          <div class="content">
            <header id="resume-header">
              <h2><span>resume</span></h2>
							<?php
							if ( $market_val = get_field('market_compensation_value') ) { ?>
								<div id="market-compensation">
									<h6>Market compensation for skill set</h6>
									<div class="market-value">
										<?php echo $market_val; ?>
										<?php
										if ( $market_label = get_field('market_compensation_label') ) { ?>
											<span><?php echo $market_label; ?></span>
										<?php
										} ?>
									</div>
	              </div>
							<?php
							} ?>
            </header>
            <h3>Selected Positions</h3>
            <div id="resume-container">
              <?php
              foreach ($resume as $res) { ?>
                <div class="res-item">
                  <div class="row">
                    <div class="res-date-container col-2">
                      <div class="res-date">
                        <span><?php echo date('Y', strtotime($res['started'])); ?></span>
                        <span class="date-divider">-</span>
                        <span><?php echo $res['ended'] ? date('Y', strtotime($res['ended'])) : 'Current'; ?></span>
                      </div>
                    </div>
                    <div class="res-content col-10">
                      <div class="row res-content-header">
                        <div class="res-title col-7">
                          <h4>
                            <?php
                            echo $res['role'];
                            echo $res['company'] ? ', ' : '';
                            if ( $res['company'] ) { ?>
                              <span><?php echo $res['company']; ?></span>
                            <?php
                            } ?>
                          </h4>
                        </div>
												<div class="res-tags col-5">
													<div id="bench-tags">
														<?php
														if ( $resume_tags = $res['resume_tags'] ) {
															foreach ( $resume_tags as $tag ) { ?>
																<benchtag class="bench-tag">
																	<?php echo $tag->name; ?>
																</benchtag>
															<?php
															}
														}
														if ( $project_type = $res['project_type'] ) { ?>
															<benchtag class="bench-tag project-type">
																<?php echo $project_type; ?>
															</benchtag>
														<?php
														} ?>
													</div>
												</div>
                      </div>
                      <?php
                      if ( $details = $res['details'] ) { ?>
                        <div class="res-details">
                          <?php
                          foreach( $details as $det ) { ?>
                            <div class="res-detail row">
                              <div class="col-2">
                                <?php echo $det['label']; ?>
                              </div>
                              <div class="col-10">
                                <?php echo $det['description']; ?>
                              </div>
                            </div>
                          <?php
                          } ?>
                        </div>
                      <?php
                      } ?>
                    </div>
                  </div>
                </div>
              <?php
              } ?>
            </div>
          </div>
        </section>
      <?php
      }

      if ( $looking_for = get_field('looking_for', 'user_' . $user_id) ) { ?>
        <section id="candidate-looking_for">
          <div class="content">
            <div id="looking-for-content">
              <h3>What they're looking for</h3>
              <?php echo $looking_for; ?>
            </div>
          </div>
        </section>
      <?php
      }

      echo get_page_form( [
        'shortcode' => '[caldera_form id="CF5d0cd99d8e145"]',
				'submit' => 'Request Details'
      ] ); ?>

		</section>

    <footer id="pre-footer">
      <div class="content">
        <div id="pre-footer-content">
          <h6>Connect with The Bench</h6>
          <?php the_field('candidate', 'option'); ?>
          <a class="cta-button pink" href="<?php echo home_url(); ?>/chat" target="_blank">Connect with The Bench</a>
        </div>
      </div>
    </footer>

		<!-- /section -->

	</main>

<?php get_footer(); ?>
