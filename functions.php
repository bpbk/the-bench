<?php
/**
 * The Bench functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package The_Bench
 */

if ( ! function_exists( 'the_bench_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function the_bench_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on The Bench, use a find and replace
		 * to change 'the-bench' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'the-bench', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		add_image_size( 'small', 400 );
		add_image_size( 'small-medium', 600 );
		add_image_size( 'medium', 800 );
		add_image_size( 'large', 1400 );
		add_image_size( 'extra-large', 1900 );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'the-bench' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'the_bench_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'the_bench_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function the_bench_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'the_bench_content_width', 640 );
}
add_action( 'after_setup_theme', 'the_bench_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function the_bench_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'the-bench' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'the-bench' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'the_bench_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function the_bench_scripts() {
	$manifest = json_decode(file_get_contents('dist/assets.json', true));
	$main = $manifest->main;

	wp_enqueue_style( 'the-bench-style', get_template_directory_uri() . $main->css, false, null );

	wp_enqueue_script('the-bench-js', get_template_directory_uri() . $main->js, ['jquery'], null, true);

	wp_enqueue_script( 'the-bench-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'the-bench-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_page_template( 'typeform.php' ) ) {
		wp_enqueue_script(
			'typescript',
			'//embed.typeform.com/embed.js',
			array( 'jquery' ),
			20192007,
			true
		);
	}
	
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'the_bench_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Custom post types
 */
require get_template_directory() . '/inc/post-types.php';

/**
 * Shortcodes
 */
require get_template_directory() . '/inc/shortcodes.php';

/**
 * SVGS
 */
require get_template_directory() . '/inc/svgs.php';

/**
 * Homepage functions
 */
require get_template_directory() . '/inc/homepage-functions.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

function slugify($text)
{
  $text = preg_replace('~[^\pL\d]+~u', '-', $text);
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
  $text = preg_replace('~[^-\w]+~', '', $text);
  $text = trim($text, '-');
  $text = preg_replace('~-+~', '-', $text);
  $text = strtolower($text);

  if (empty($text)) {
    return 'n-a';
  }

  return $text;
}

function get_page_form ( $form ) {
	ob_start(); ?>
	<section class="page-form-section <?php echo isset($form['color']) ? $form['color'] : 'blue'; ?>">
		<div class="content">
			<div
				class="page-form"
				data-submit="<?php echo $form['submit'] ? $form['submit'] : ''; ?>"
				data-header="<?php echo $form['header'] ? $form['header'] : ''; ?>">
				<?php echo do_shortcode($form['shortcode']); ?>
			</div>
		</div>
	</section>
	<?php
	return ob_get_clean();
}

// Registering custom post status
function add_archived_post_status(){
  register_post_status('archived', [
	  'label'                     => _x( 'Archived', 'post' ),
	  'public'                    => true,
	  'exclude_from_search'       => true,
	  'show_in_admin_all_list'    => true,
	  'show_in_admin_status_list' => true,
		'post_type'                 => [ 'opportunty' ],
	  'label_count'               => _n_noop( 'Archived <span class="count">(%s)</span>', 'Archived <span class="count">(%s)</span>' ),
  ] );
}

add_action( 'init', 'add_archived_post_status' );

// Using jQuery to add it to post status dropdown
add_action('admin_footer-post.php', 'append_archived_post_status_list');

function append_archived_post_status_list(){

	global $post;
	$complete = '';
	$label = '';
	if ( $post->post_type == 'opportunity' ) {
		if ( $post->post_status == 'archived' ) {
			$complete = ' selected="selected"';
			$label = '<span id="post-status-display"> Archived</span>';
			echo '
			<script>
				$("span#post-status-display").html("Archived");
				$("input#save-post").val("Save Archived");
			</script>
			';
		}
		echo '
		<script>
			jQuery(document).ready( function($) {
				$("select#post_status").append("<option value=\"archived\" '.$complete.'>Archived</option>");
				$(".misc-pub-section label").append("'.$label.'");
			});
		</script>
		';
	}
}

add_role(
    'candidate',
    __( 'Candidate' ),
    array(
        'read'         => false,  // true allows this capability
        'edit_posts'   => false,
    )
);

add_action( 'user_new_form', 'dontchecknotify_register_form' );

function dontchecknotify_register_form() {
    echo '<scr'.'ipt>
    jQuery(document).ready(function($) {
        $("#send_user_notification").removeAttr("checked");
    } );
    </scr'.'ipt>';
}

add_filter( 'request', 'set_candidate_id' );

function set_candidate_id( $query_vars ) {
  if ( array_key_exists( 'author_name', $query_vars ) ) {
    global $wpdb;
		// $results = $wpdb->get_results( "ALTER TABLE {$wpdb->usermeta} AUTO_INCREMENT=1001" );
    $author_id = $query_vars['author_name'];
		$user = get_userdata( $author_id );
		if ( $author_id ) {
			if ( in_array( 'candidate', (array) $user->roles ) ) {
				$query_vars['author'] = $author_id;
	      unset( $query_vars['nickname'] );
				unset( $query_vars['author_name'] );
			}
		}
  }
  return $query_vars;
}

add_filter( 'author_link', 'set_candidate_author_link', 10, 3 );

function set_candidate_author_link( $link, $author_id, $author_nicename ) {
	$user = get_userdata( $author_id );
	if ( in_array( 'candidate', (array) $user->roles ) ) {
    $link = str_replace( $author_nicename, $author_id, $link );
	}
  return $link;
}

add_action('init', 'set_candidate_slug');

function set_candidate_slug() {
  global $wp_rewrite;
  $author_slug = 'candidate'; // change slug name
  $wp_rewrite->author_base = $author_slug;
}

function change_opportunity_link( $post_link, $post = 0 ) {
  if ( $post->post_type === 'opportunity' ) {
    return home_url( 'opportunity/' . $post->ID . '/' );
  } else {
    return $post_link;
  }
}

add_filter('post_type_link', 'change_opportunity_link', 1, 3);

function opportunity_rewrites_init () {
  add_rewrite_rule( 'opportunity/([0-9]+)?$', 'index.php?post_type=opportunity&p=$matches[1]', 'top' );
}

add_action('init', 'opportunity_rewrites_init');

if ( !function_exists('log_it') ) {
	function log_it( $message ) {
		if( WP_DEBUG === true ){
			if( is_array( $message ) || is_object( $message ) ){
				error_log( print_r( $message, true ) );
			} else {
				error_log( $message );
			}
		}
	}
}

add_filter( 'pre_get_document_title', 'filter_page_title_for_author' );

function filter_page_title_for_author($title) {
	if ( is_author() ) {
		global $wp_query;
		$author_id = $wp_query->query['author'];
		$title = 'Candidate #' . $author_id . ' | ' . get_bloginfo('name');
	}
  return $title;
}

function compare_experience($array1, $array2) {
  return $array1['years'] < $array2['years'];
}

add_filter( 'caldera_forms_do_magic_tag', 'slug_id_to_taxonomy_name', 10, 2 );
function slug_id_to_taxonomy_name( $value, $magic_tag  ) {
  if ( '{candidate}' == $magic_tag ) {
		$user = get_queried_object();
		$user_id = $user->ID;
    $value = $user_id;
  }
	if ( '{candidate_url}' == $magic_tag ) {
		$user = get_queried_object();
		$user_id = $user->ID;
		$candidate_link = home_url() . '/candidate/' . $user_id;
		$value = $candidate_link;
	}
	if ( '{opportunity}' == $magic_tag ) {
		global $post;
		$opportunity = $post->ID;
    $value = $opportunity;
  }
  return $value;
}

add_filter('manage_edit-opportunity_columns', 'opportunities_columns_id', 5);
add_action('manage_opportunity_posts_custom_column', 'opportunities_custom_id_columns', 5, 2);

function opportunities_columns_id($defaults){
	$defaults['wps_post_id'] = '<p style="text-align: center">' . __('Opportunity #') . '</p>';
	$defaults['opp_archived'] = '<p style="text-align: center">' . __('Archived') . '</p>';
	$defaults['opp_company'] = '<p style="text-align: center">' . __('Company') . '</p>';
  return $defaults;
}
function opportunities_custom_id_columns($column_name, $id){
  if( $column_name === 'wps_post_id'){
    echo '<p style="text-align: center">' . $id . '</p>';
  }
	if( $column_name === 'opp_archived' && get_post_status($id) === 'archived' ){
    echo '<p style="text-align: center">✓</p>';
  }
	if( $column_name === 'opp_company' && $company = get_field('company', $id) ){
    echo '<p style="text-align: center">' . $company . '</p>';
  }
}

function modify_candidate_table( $column ) {
  $column['candidate'] = '<p style="text-align: center;">Candidate #</p>';
  return $column;
}
add_filter( 'manage_users_columns', 'modify_candidate_table' );

function modify_candidate_table_row( $val, $column_name, $user_id ) {
  switch ($column_name) {
    case 'candidate' :
      return '<p style="text-align: center;">' . $user_id . '</p>';
    default:
  }
  return $val;
}
add_filter( 'manage_users_custom_column', 'modify_candidate_table_row', 10, 3 );

function create_company_member($member) { ?>
	<div class="co-member-content">
		<?php
		if ( $member['linkedin'] ) { ?>
			<a class="co-member-linkedin" target="_blank" href="<?php echo $member['linkedin']; ?>">
				<i class="fab fa-linkedin-in"></i>
			</a>
		<?php
		}
		if ( $member['image'] ) { ?>
			<div class="co-member-image">
				<img src="<?php echo $member['image']['sizes']['small']; ?>"/>
			</div>
		<?php
		}
		if ( $member['title'] ) { ?>
			<span class="co-member-title"><?php echo $member['title']; ?></span>
		<?php
		}
		if ( $member['name'] ) { ?>
			<h5><?php echo $member['name']; ?></h5>
		<?php
		}
		if ( $member['work_history'] ) { ?>
			<div class="co-member-work-history">
				<p><strong>Previously:</strong></p>
				<p><?php echo $member['work_history']; ?></p>
			</div>
		<?php
		} ?>
	</div>
	<?php
	if ( $member['number_of_designers'] || $member['the_bench_openings'] ) { ?>
		<div class="co-member-info">
			<?php if ( $member['the_bench_openings'] ) { ?>
				<div class="co-member-openings">
					<span>+<?php echo $member['the_bench_openings']; ?></span>
				</div>
			<?php } ?>
			<span class="co-member-designers"><?php echo $member['number_of_designers'] ? $member['number_of_designers'] : 0; ?></span>
		</div>
		<span class="co-member-info-label">Designers</span>
	<?php
	}
}

// ACF Filter to add company name to more opportunities select

function more_opportunities_result( $title, $post, $field, $post_id ) {

  if ( $company = get_field('company', $post->ID) ) {
		$title .= ' - ' . $company;
	}

  return '#' . $post->ID . ': ' . $title;
}


// filter for every field
add_filter('acf/fields/relationship/result/name=more_opportunities', 'more_opportunities_result', 10, 4);

// Add menu choices to SVG select
function acf_svg_menu_choices( $field ) {

  // reset choices
  $field['choices'] = [
		'sign-up-form' => 'Sign-up Form',
		'phone-alert' => 'Phone Alert',
		'interview' => 'Interview',
		'concierge' => 'Concierge',
		'website' => 'Website'
	];

  return $field;

}

add_filter('acf/load_field/name=svg_selection', 'acf_svg_menu_choices');

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page();

}
