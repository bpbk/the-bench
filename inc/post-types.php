<?php
function register_custom_post_types() {

	$labels = array(
		'name'                       => _x( 'Company Values', 'taxonomy general name', 'iongeo' ),
		'singular_name'              => _x( 'Company Value', 'taxonomy singular name', 'iongeo' ),
		'search_items'               => __( 'Search Company Values', 'iongeo' ),
		'popular_items'              => __( 'Popular Company Values', 'iongeo' ),
		'all_items'                  => __( 'All Company Values', 'iongeo' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Company Value', 'iongeo' ),
		'update_item'                => __( 'Update Company Value', 'iongeo' ),
		'add_new_item'               => __( 'Add New Company Value', 'iongeo' ),
		'new_item_name'              => __( 'New Company Value Name', 'iongeo' ),
		'separate_items_with_commas' => __( 'Separate company values with commas', 'iongeo' ),
		'add_or_remove_items'        => __( 'Add or remove company values', 'iongeo' ),
		'choose_from_most_used'      => __( 'Choose from the most used company values', 'iongeo' ),
		'not_found'                  => __( 'No company values found.', 'iongeo' ),
		'menu_name'                  => __( 'Company Values', 'iongeo' ),
	);
	$args = array(
    'show_tagcloud'         => true,
		'hierarchical'          => true,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'show_in_nav_menus'     => true,
    'show_in_rest'          => true,
		'public'								=> true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'								=> true,
	);

	register_taxonomy( 'company-value', array('opportunity'), $args );
	register_taxonomy_for_object_type( 'company-value', array('opportunity') );

	$labels = array(
    'name' => _x('Opportunity', 'post type general name'),
    'singular_name' => _x('Opportunity', 'post type singular name'),
    'add_new' => _x('Add New', 'post type add new'),
    'add_new_item' => __('Add New Opportunity'),
    'edit_item' => __('Edit Opportunity'),
    'new_item' => __('New Opportunity'),
    'view_item' => __('View Opportunities'),
    'search_items' => __('Search Opportunities'),
    'not_found' =>  __('Nothing found'),
    'not_found_in_trash' => __('Nothing found in Trash'),
    'parent_item_colon' => ''
  );

  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'hierarchical' => true,
    'menu_position' => 5,
    'has_archive' => false,
    'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'author'),
    'taxonomies' => array('company-value'),
  );

  register_post_type( 'opportunity' , $args );
}

add_action('init', 'register_custom_post_types'); ?>
