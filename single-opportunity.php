<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package The_Bench
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();
			$company = get_field('company');
			$status = get_post_status(); ?>
			<div class="navigation-container">
				<nav id="site-navigation" class="main-navigation">
					<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'the-bench' ); ?></button>
					<a href="#organization">Organization</a>
					<?php
					if ( get_field('role_overview') ) { ?>
						<a href="#overview">The Role</a>
					<?php
					} ?>
					<a href="#our-rating">Our Rating</a>
				</nav><!-- #site-navigation -->
			</div>
			<header id="opportunity-header" class="bg-light-blue header-padding">

				<?php
				if ( $status == 'archived' ) { ?>

					<div class="content archived header-padding">
						<h6>Opportunity Brief</h6>
						<h2><span>This opportunity</span> has passed.</h2>
						<p>The position was originally posted on <?php the_date(); ?> and has now been filled.</p>
							<a class="cta-button dark-blue long" target="_blank" href="<?php echo get_site_url(); ?>/more-opportunities">
								More opportunities
							</a>
					</div>

					<?php echo get_svg_by_name('phone-alert'); ?>

					<?php
					echo get_page_form( [
						'shortcode' => '[caldera_form id="CF5d0cdaa0aa3eb"] ',
						'color' => 'blue',
						'header' => 'Interested in Design Opportunities?'
					] );
				} else {
					$title_arr = preg_split( '/\s+/', get_the_title() );
					$title_start = $title_arr[0];
					array_shift( $title_arr );
					$title = get_the_title();
					$excerpt = get_the_excerpt();
					$permalink = get_permalink();
					$email_subject = rawurlencode($title . ' position from The Bench');
					$email_body = rawurlencode('I thought you might be interested in this ' . $title . ' position from The Bench -- ' . $permalink);
					$title_end = implode( " ", $title_arr ); ?>
					<div class="content header-padding">
						<div id="share-opportunity-container">
							<div id="share-opportunity">
								<div id="share-opportunity-content">
									<span>share</span>
								</div>
							</div>
							<div id="share-opportunity-links">
								<a
									href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $permalink; ?>&title=<?php echo $title; ?>&summary=<?php echo $excerpt; ?>"
									rel="noopener"
									target="_blank"
									class="social-link">
									<span class="icon">
										<i class="fab fa-linkedin-in"></i>
									</span>
								</a>
								<a
									href="http://www.facebook.com/sharer.php?s=100&amp;p[title]=<?php echo $title; ?>&amp;p[summary]=<?php echo $excerpt; ?>&amp;p[url]=<?php echo $permalink; ?>&amp;p[images[0]=<?php echo $blog_image; ?>"
									onclick="window.open(this.href, 'facebookwindow','left=20,top=20,width=600,height=700,toolbar=0,resizable=1'); return false;"
									class="social-link">
									<span class="icon">
										<i class="fab fa-facebook-f"></i>
									</span>
								</a>
								<a
									href="http://twitter.com/intent/tweet?text=<?php echo $title; ?>"
									onclick="window.open(this.href, 'twitterwindow','left=20,top=20,width=600,height=300,toolbar=0,resizable=1'); return false;"
									class="social-link">
									<span class="icon">
										<i class="fab fa-twitter"></i>
									</span>
								</a>
								<a
									href="mailto:?subject=<?php echo $email_subject; ?>&body=<?php echo $email_body; ?>"
									class="social-link">
									<span class="icon">
										<i class="fas fa-envelope"></i>
									</span>
								</a>
							</div>
						</div>
						<h6>Opportunity Brief</h6>
						<h1>
							<span><?php echo $title_start; ?></span>
							<?php echo $title_end; ?>
						</h1>
						<h3 class="young"><?php echo $company; ?></h3>
						<div class="row justify-between">
							<div class="col-5">
								<?php
								if ( $co_overview = get_field('company_overview') ) {
									echo $co_overview;
								}
								if ( $co_website = get_field('company_website') ) { ?>
									<a class="cta-button dark-blue long website" target="<?php echo $co_website['target']; ?>" href="<?php echo $co_website['url']; ?>">
										<?php echo isset($co_website['text']) ? $co_website['text'] : 'Visit Site'; ?>
									</a>
								<?php
								} ?>
							</div>
							<div class="col-7 image illustration-container">
								<img src="<?php echo get_template_directory_uri(); ?>/images/illustrations/job.svg">
							</div>
						</div>
					</div>
				<?php
				} ?>
			</header>

			<?php
			if ( $status != 'archived' ) {

				echo get_page_form( [
					'shortcode' => '[caldera_form id="CF5d0cdaa0aa3eb"] ',
					'color' => 'blue'
				] );

				//Organization Section ?>

				<section id="opportunity-organization">
					<a class="anchor-tag" name="organization"></a>
					<div class="content">
						<h2 class="text-centered lowercase"><span>The organization</span></h2>
					</div>

					<?php
					if ( $co_members = get_field('company_members') ) { ?>
						<div id="co-members" class="members-slider">
							<div class="content">
								<div>
									<div class="the-bench-arrows">
										<div class="arrow prev-arrow">
											<img src="<?php echo get_template_directory_uri(); ?>/images/ui/arrow.svg"/>
										</div>
										<div class="arrow next-arrow">
											<img src="<?php echo get_template_directory_uri(); ?>/images/ui/arrow.svg"/>
										</div>
									</div>
									<div class="row co-members-row">
										<?php
										foreach ( $co_members as $member ) { ?>
											<div class="co-member col-4<?php echo $member['sub_members'] ? ' has-sub-members' : ''; ?>">
												<?php
												create_company_member($member);
											  ?>
											</div>
										<?php
										} ?>
									</div>
								</div>
								<?php
								foreach ( $co_members as $member ) {
									if ( $sub_members = $member['sub_members'] ) { ?>
										<div>
											<div class="the-bench-arrows">
												<div class="arrow prev-arrow">
													<img src="<?php echo get_template_directory_uri(); ?>/images/ui/arrow.svg"/>
												</div>
												<div class="arrow next-arrow">
													<img src="<?php echo get_template_directory_uri(); ?>/images/ui/arrow.svg"/>
												</div>
											</div>
											<div class="row sub-member-row">
												<?php
												foreach ( $sub_members as $sub_member ) { ?>
													<div class="co-member col-4">
														<?php create_company_member($sub_member); ?>
													</div>
												<?php
												} ?>
											</div>
										</div>
									<?php
									}
								} ?>
							</div>
						</div>
					<?php
					}

					if ( $co_background  = get_field('company_bg') ) { ?>
						<div id="company-background" class="content">
							<h3>Company Background</h3>
							<div class="row justify-between">
								<div class="col-5">
									<?php
									if ( $co_tags = get_field('company_tags') ) { ?>
										<div id="bench-tags">
											<?php
											foreach ( $co_tags as $tag ) { ?>
												<benchtag class="bench-tag">
													<?php echo $tag->name; ?>
												</benchtag>
											<?php
											} ?>
										</div>
									<?php
									}
									echo $co_background; ?>
								</div>
								<div class="col-7 illustration-container">
									<img src="<?php echo get_template_directory_uri(); ?>/images/illustrations/organization.svg">
								</div>
							</div>
							<?php
							if ( $timeline = get_field('timeline') ) { ?>
								<h3>Select Milestones</h3>
								<div id="timeline-container">
									<div id="timeline">
										<?php
										foreach ( $timeline as $tl ) { ?>
											<div class="timeline-item-container">
												<div class="timeline-item">
													<p><?php echo $tl['details']; ?></p>
													<span></span>
													<label><?php echo $tl['year']; ?></label>
												</div>
											</div>
										<?php
										} ?>
									</div>
									<div class="the-bench-arrows">
										<div class="arrow prev-arrow">
											<img src="<?php echo get_template_directory_uri(); ?>/images/ui/arrow.svg"/>
										</div>
										<div class="arrow next-arrow">
											<img src="<?php echo get_template_directory_uri(); ?>/images/ui/arrow.svg"/>
										</div>
									</div>
								</div>
								</div>
							<?php
							} ?>
						</div>
					<?php
					} ?>
				</section>

				<section id="the-role">
					<a class="anchor-tag" name="overview"></a>
					<div class="the-content">
						<h2 class="lowercase text-centered"><span>The Role</span></h2>
					</div>
					<?php
					if ( $role_overview  = get_field('role_overview') ) { ?>
						<div id="role-overview" class="content">
							<h3>Position Overview</h3>
							<div class="row justify-between">
								<div class="col-5">
									<?php echo $role_overview; ?>
								</div>
								<div class="col-7 illustration-container">
									<img src="<?php echo get_template_directory_uri(); ?>/images/illustrations/role.svg">
								</div>
							</div>
						</div>
					<?php
					} ?>
				</section>

				<?php
				if ( $co_partners = get_field('collaboration_partners') ) { ?>

					<section id="main-collaboration-partners">
						<a class="anchor-tag" name="main-collaboration-partners"></a>
						<div class="content">
							<h3 class="text-centered lowercase">Main Collaboration Partners</h3>
						</div>

							<div id="col-members" class="members-slider">
								<div class="content">
									<div class="the-bench-arrows">
										<div class="arrow prev-arrow">
											<img src="<?php echo get_template_directory_uri(); ?>/images/ui/arrow.svg"/>
										</div>
										<div class="arrow next-arrow">
											<img src="<?php echo get_template_directory_uri(); ?>/images/ui/arrow.svg"/>
										</div>
									</div>
									<div class="row">
										<?php
										foreach ( $co_partners as $member ) { ?>
											<div class="co-member col-4">
												<?php
												create_company_member($member);
												?>
											</div>
										<?php } ?>
									</div>
								</div>
							</div>

					</section>
				<?php } ?>

				<section id="why-we-like" class="bg-light-blue">
					<a class="anchor-tag" name="our-rating"></a>
					<div class="content">
						<h2><span>why we like this opportunity</span></h2>
						<div class="row">
							<div class="col-4">
								<?php
								if ( $values = get_the_terms( get_the_ID(), 'company-value' ) ) { ?>
									<div id="company-values">
										<?php
										foreach ( $values as $value ) { ?>
											<div class="company-value">
												<img src="<?php echo get_field( 'icon', $value )['sizes']['small']; ?>"/>
												<p><?php echo $value->name; ?></p>
											</div>
										<?php
										} ?>
									</div>
								<?php
								} ?>
							</div>
							<div class="col-7">
								<h3 class="young"><?php the_title(); ?></h3>
								<h4><?php echo $company; ?></h4>
								<?php
								if ( $project_type = get_field('project_type') ) { ?>
									<benchtag class="bench-tag project-type"><?php echo $project_type; ?></benchtag>
								<?php
								}
								if ( $why_we_like = get_field('why_we_like') ) {
									echo $why_we_like;
								} ?>
							</div>
						</div>
					</div>
				</section>

				<?php
				if ( $processes = get_field('interview_process') ) { ?>
					<?php
					// echo get_page_form( [
		      //   'title' => 'Interested in this opportunity?',
		      //   'color' => 'blue',
		      //   'link' => [
		      //     'url' => 'https://google.com',
		      //     'title' => 'Discuss with The Bench'
		      //   ]
		      // ] );
					echo get_page_form( [
		        'shortcode' => '[caldera_form id="CF5d0cdaa0aa3eb"] ',
		        'color' => 'blue'
		      ] ); ?>

					<section id="interview-process">
						<div class="content">
							<header>
								<h2><span>the interview process</span></h2>
								<?php
								if ( $subtitle = get_field('interview_subtitle') ) {
									echo $subtitle;
								}
								?>
							</header>
							<div class="processes row">
								<?php
								foreach( $processes as $key => $process) { ?>
									<div class="process col-4">
										<h6><?php echo $process['round'] ? $process['round'] : 'Round ' . ((int)$key + 1); ?></h6>
										<?php
										if ( $process['round_type'] ) { ?>
											<span class="round-type">
												<?php echo $process['round_type']; ?>
											</span>
										<?php
										}
										if ( $interviewers = $process['interviewer'] ) {
											echo $interviewers;
										}
										if ( $type = $process['type'] ) { ?>
											<button
												class="interview-type<?php echo $process['type_description'] ? ' tool-tip' : ''; ?>"
												data-tool-tip="<?php echo $process['type_description'] ? $process['type_description'] : ''; ?>"><?php echo $type; ?></button>
										<?php
										} ?>
									</div>
								<?php
								} ?>
							</div>
						</div>
					</section>
				<?php
				}
			} // End not archived

		endwhile; // End of the loop.
		?>

		<footer id="pre-footer">
      <div class="content">
        <div id="pre-footer-content">
          <h6>Connect with The Bench</h6>

					<?php
					if ( $status == 'archived' ) { ?>
	          <?php the_field('opportunity_passed', 'option'); ?>
					<?php }
					else {
					?>
						<?php the_field('opportunity_active', 'option'); ?>
					<?php } ?>
          <a class="cta-button pink" href="<?php echo home_url(); ?>/chat" target="_blank">Connect with The Bench</a>
        </div>
      </div>
    </footer>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
