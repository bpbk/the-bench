<?php
/**
 * Template Name:  Typeform
 *
 * The template for displaying more opportunities
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package The Bench
 */

get_header();
?>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-144167738-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-144167738-1');
</script>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<?php
			while ( have_posts() ) :
				the_post();
				?>
				<div class="entry-content">
					<?php
						$typeform = get_field( 'typeform_number' );
						$width    = get_field( 'width' );
						$height   = get_field( 'height' );
					if ( $typeform ) {
						?>
						<!-- <div class="typeform-widget" data-url="https://thebench.typeform.com/to/<?php echo esc_html( $typeform ); ?>" style="width: <?php echo wp_kses_post( $width ); ?>; height:<?php echo wp_kses_post( $height ); ?>;"></div>
						<script> (function() { var qs,js,q,s,d=document, gi=d.getElementById, ce=d.createElement, gt=d.getElementsByTagName, id="typef_orm", b="https://embed.typeform.com/"; if(!gi.call(d,id)) { js=ce.call(d,"script"); js.id=id; js.src=b+"embed.js"; q=gt.call(d,"script")[0]; q.parentNode.insertBefore(js,q) } })() </script> -->
						<style type="text/css"> html{ margin: 0; height: 100%; overflow: hidden; } iframe{ position: absolute; left:0; right:0; bottom:0; top:0; border:0; } </style>
						<iframe id="typeform-full" width="<?php echo wp_kses_post( $width ); ?>" height="<?php echo wp_kses_post( $height ); ?>" frameborder="0" src="https://thebench.typeform.com/to/<?php echo esc_html( $typeform ); ?>"></iframe>
						<?php
					}
					?>
					<?php
					the_content();

					wp_link_pages(
						array(
							'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'foundry-starter-theme' ),
							'after'  => '</div>',
						)
					);
					?>
				</div><!-- .entry-content -->
				<?php
				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
