<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package The_Bench
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<?php
			if ( have_posts() ) :

				if ( is_home() && ! is_front_page() ) :
					?>
					<header>
						<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
					</header>
					<?php
				endif;

				/* Start the Loop */
				while ( have_posts() ) :
					the_post();
					$content = get_post( get_option( 'page_on_front' ) ); ?>

					<div id="homepage-hero">
						<div class="content">
							<div class="row">
								<div class="col-10 full-width">
									<?php echo $content->post_content; ?>
									<a class="cta-button long dark-blue">get notified</a>
								</div>
							</div>
						</div>
					</div>
					<?php
					if ( $sections = get_field('sections', get_option('page_on_front')) ) {
            foreach( $sections as $section ) {
							if ( $section['menu_title'] ) { ?>
								<a name="<?php echo slugify($section['menu_title']); ?>"></a>
							<?php
							} ?>
              <section class="<?php echo $section['acf_fc_layout']; ?>-wrapper homepage-section">
								<?php
                echo call_user_func('get_' . $section['acf_fc_layout'] . '_section', $section); ?>
              </section>
            <?php
            }
          }

				endwhile;

			endif;
			?>

			<footer id="footer-signup" class='dark'>
				<div class="content">
					<h6>Start your search</h6>
					<div id="form-search-toggle">
						<div class="caldera-grid">
							<div class="row">
								<div class="single">
									<p>Hello! I'm looking for </p>
									<select>
										<option value="opportunities">Product Design Opportunities</option>
										<option value="talent">Product Design Talent</option>
									</select>
									<p> .</p>
								</div>
							</div>
						</div>
					</div>
					<div id="footer-form-opportunities" class="footer-mad-lib">
						<?php echo do_shortcode('[caldera_form id="CF5d0972e99bbef"]'); ?>
					</div>
					<div id="footer-form-talent" class="footer-mad-lib">
						<?php echo do_shortcode('[caldera_form id="CF5d28894be7a27"]'); ?>
					</div>
				</div>
			</footer>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
