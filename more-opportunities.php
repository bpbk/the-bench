<?php
/**
 * Template Name:  More Opportunities
 *
 * The template for displaying more opportunities
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package The Bench
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="content header-padding">
				<h2><span>More </span>opportunities...</h2>
			</div>
			<?php
			$args = [
				'post_type' => 'opportunity',
				'post_status' => 'publish',
			];
			while ( have_posts() ) :
				the_post();
					if ( $more_opps = get_field('more_opportunities') ) {
						$args['post__in'] = $more_opps;
					}
				endwhile;
			$opportunites_query = new WP_Query($args);
			if($opportunites_query->have_posts()) : ?>
				<div id="more-opps-container">
					<div id="more-opps">
						<?php
				    while($opportunites_query->have_posts()): $opportunites_query->the_post();
							$company = get_field('company');
							$image = get_the_post_thumbnail_url(get_the_ID(), 'large'); ?>
				      <div class="opp-item">
								<div class="opp-item-image bg-centered" style="background-image:url(<?php echo get_template_directory_uri(); ?>/images/illustrations/city.svg);">
									<?php
									if ( $project_type = get_field('project_type') ) { ?>
										<benchtag class="bench-tag project-type"><?php echo $project_type; ?></benchtag>
									<?php
									} ?>
									<a href="<?php the_permalink(); ?>" class="cta-button dark-blue-bg">Learn More</a>
								</div>
								<div class="opp-item-content">
									<div class="row">
										<?php
										if ( $company_logo = get_field('company_logo') ) { ?>
											<div class="col-2">
												<div class="opp-item-icon-wrapper">
													<div class="opp-item-icon-container">
														<div class="opp-item-icon">
															<img src="<?php echo $company_logo['sizes']['small-medium']; ?>"/>
														</div>
													</div>
												</div>
											</div>
										<?php
										} ?>
										<div class="opp-item-info col-10">
											<h6 class="uppercase opp-item-title"><?php the_title(); ?></h6>
							      	<h3 class="young"><?php echo $company; ?></h3>
											<?php
											if ( $co_overview = get_field('company_overview') ) {
												echo $co_overview;
											}
											echo get_the_date(); ?>
										</div>
									</div>
								</div>
				      </div>
				    <?php
				    endwhile;
				    wp_reset_postdata(); ?>
					</div>
				</div>
				<div id="more-opp-arrows" class="the-bench-arrows">
					<div class="arrow prev-arrow">
						<img src="<?php echo get_template_directory_uri(); ?>/images/ui/arrow.svg"/>
					</div>
					<div class="arrow next-arrow">
						<img src="<?php echo get_template_directory_uri(); ?>/images/ui/arrow.svg"/>
					</div>
				</div>

			<?php
			else: ?>
				<div class="content">
					<h3 class="young">No opportunities right now</h3>
				</div>
			<?php
		  endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
