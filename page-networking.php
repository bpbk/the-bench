<?php
/**
* Template Name: Networking Page
*
**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package The_Bench
 */

get_header();
?>
<style>
.network-member-row {
	width: 72%;
	margin: 0 auto;
    padding-bottom: 75px;
}
.network-member {
	text-align: center;
    padding-left: 70px;
    padding-right: 70px;
    margin-bottom: 75px;
    margin-top: 50px;
}
.network-member .image {
    width:150px;
    border-radius: 100px;
}
.network-member h6 {
	font-family: "neuzeit-grotesk";
	font-size:14px;
	color:#404040;
	margin-bottom: 0px;
	margin-top: 20px;
	font-weight:100;
}
.network-member h2 {
	font-size:calc(1.414rem * 1.4);
	font-family: "neuzeit-grotesk";
    font-weight: 600;
	color: #01063a;
	margin-top: 5px;
    margin-bottom: 5px;
}
.network-member h3 {
	font-family: "YoungSerif";
	font-size:20px;
	color: #01063a;
    opacity: .5;
	margin-bottom: 5px;
	margin-top:5px;
}
.network-member .social {
    color: #01063a;
    font-size: 20px;
    float: right;
}
.network-member .social:hover {
	opacity:0.8;
}
.network-member .hiring {
	font-size:15px;
}
.network-member-content {
	border: 1px solid;
	padding-left: 20px;
    padding-right: 20px;
	padding-top: 20px;
	min-height:400px;
}
.the-network-content {
	padding-top:50px;
}
.connect-button {
	text-transform: uppercase;
    border: 1px solid #e49589;
    color: #e49589 !important;
    font-size: calc(.707rem * 1.2);
    font-weight: 600;
    padding: .75em 4em .5em;
    border-radius: 0;
    background-color: transparent;
    margin-top: 2em;
    text-decoration: none;
}
.footer-p {
	font-size: 28px !important;
    margin-bottom: 20px !important;
}
#footer-signup .title {
	margin-bottom:30px !important;
}
@media (max-width:768px) {
	.the-network-content h2 {
		text-align:center;
	}
	.network-member {
		padding-left:0px;
		padding-right:0px;
	}
}
</style>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-144167738-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-144167738-1');
</script>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<?php
			if ( have_posts() ) :

				/* Start the Loop */
				while ( have_posts() ) :
					the_post();
					$content = get_post(); ?>

					<div id="homepage-hero">
						<div class="content">
							<div class="row">
								<div class="col-10 full-width">
									<?php echo $content->post_content; ?>
									<a class="cta-button long dark-blue">get notified</a>
								</div>
							</div>
						</div>
					</div>
					<div class="content the-network-content">
						<h2>The Network</h2>
					</div>
					<div class="row network-member-row">
					<?php 
					// check if the repeater field has rows of data
					if( have_rows('networking_member') ):
						 // loop through the rows of data 
						while ( have_rows('networking_member') ) : the_row();
							// display a sub field value
							?>
							<div class="col-4 network-member">
								<div class="network-member-content">
									<a class="social" target="_blank" href="<?php the_sub_field('linkedin');?>"><i class="fab fa-linkedin-in"></i></a><br />
									<img class="image" src="<?php the_sub_field('image');?>" />
									<h6 class="name"><?php the_sub_field('position');?></h6>
									<h2 class="name"><?php the_sub_field('name');?></h2>
									<h3 class="current-company"><?php the_sub_field('current_company');?></h3>
									<p class="hiring"><?php the_sub_field('currently_hiring');?></p>
								</div>
							</div>
							<?php 
						endwhile;
					
					else :
					
						// no rows found
					
					endif;
				?> </div> <?php	
				endwhile;

			endif;
			?>

			<footer id="footer-signup" class='dark'>
				<div class="content">
					<h6 class="title"><?php the_field('title');?></h6>
						<div class="caldera-grid">
							<div class="row">
								<div class="single">
									<p class="footer-p"><?php the_field('question');?></p>
								</div>
								<div class="single">
									<p class="footer-p"><?php the_field('paragraph');?></p>
								</div>
								<div class="single">
									<a class="connect-button" href="<?php the_field('button_link');?>"><?php the_field('button_text');?></a>
								</div>
						</div>
					</div>
				</div>
			</footer>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
