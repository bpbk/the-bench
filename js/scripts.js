import $ from 'jquery';
import 'slick-carousel';

if ( $('#form-search-toggle').length ) {
  $('#form-search-toggle select').on('change', function() {
    var selectedSearch = $('#form-search-toggle select').find(":selected").val()
    console.log(selectedSearch);
    $('.footer-mad-lib').hide();
    $(`#footer-form-${selectedSearch}`).show();
  })
}

if ( $('#timeline').length ) {
  $('#timeline').slick({
    slidesToShow: 3,
    slidesToScroll: 3,
    prevArrow: $('#timeline-container .prev-arrow'),
    nextArrow: $('#timeline-container .next-arrow'),
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      }
    ]
  })
}

if ( $('#more-opps').length ) {
  $('#more-opps').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    centerMode: true,
    variableWidth: true,
    prevArrow: $('#more-opp-arrows .prev-arrow'),
    nextArrow: $('#more-opp-arrows .next-arrow'),
    responsive: [
      {
        breakpoint: 768,
        settings: {
          variableWidth: false,
          centerMode: false
        }
      }
    ]
  })
}

if ( $('#opportunities-slider').length ) {
  $('#opportunities-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots: true,
    fade: true,
    appendDots: $('.opportunities'),
    customPaging: function( slider, i ) {
      return '<div><span>' + $('.opportunity-slide', slider.$slides[i]).data('title') + '</span></div>';
    },
  }).on("beforeChange", function (event, slick, currentSlide, nextSlide) {
    var color = $('.opportunity-slide', slick.$slides[nextSlide]).data('color');
    console.log(color)
    $('.opportunity-slide-bg .st0').css('fill', color)
  })
}

if ( $('.members-slider').length ) {
  $('.members-slider').each( function() {
    const $coMembersRow = $(this).find('.co-members-row')
    const $subMembersRow = $(this).find('.sub-member-row')
    if ( $(window).width() < 768 ) {
      slickifyMembers($coMembersRow);
      slickifyMembers($subMembersRow);
    }
    $(window).resize(function(){
      if ( $(window).width() < 768 ) {
        if ( !$($coMembersRow).hasClass('slick-initialized') ) {
          slickifyMembers($coMembersRow);
          slickifyMembers($subMembersRow);
        }
      } else {
        if ( $($coMembersRow).hasClass('slick-initialized') ) {
          $($coMembersRow).slick('unslick');
          $($subMembersRow).slick('unslick');
        }
      }
    });
  })
}
// if ( $('#co-members').length ) {
//   if ( $(window).width() < 768 ) {
//     slickifyMembers($('#co-members'));
//   }
//   $(window).resize(function(){
//     if ( $(window).width() < 768 && !$('#co-members .row').hasClass('slick-initialized') ) {
//       slickifyMembers();
//     } else {
//       if ( $('#co-members .row').hasClass('slick-initialized') ) {
//         //$('#co-members .row').slick('unslick');
//       }
//     }
//   });
// }

// if ( $('#col-members').length ) {
//   if ( $(window).width() < 768 ) {
//     slickifyMembers($('#col-members'));
//   }
//   $(window).resize(function(){
//     if ( $(window).width() < 480 && !$('#col-members .row').hasClass('slick-initialized') ) {
//       slickifyMembers();
//     } else {
//       if ( $('#col-members .row').hasClass('slick-initialized') ) {
//         //$('#co-members .row').slick('unslick');
//       }
//     }
//   });
// }

function slickifyMembers ( $container ) {
  $($container).slick({
    slidesToShow: 2,
    slidesToScroll: 2,
    arrows: true,
    dots: false,
    mobileFirst: false,
    adaptiveHeight: true,
    prevArrow: $($container).parent().find('.prev-arrow'),
    nextArrow: $($container).parent().find('.next-arrow'),
    responsive: [
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  })
}

$.fn.isInViewport = function() {
  var elementTop = $(this).offset().top + ($(window).height() / 3);
  var elementBottom = elementTop + $(this).outerHeight();
  var viewportTop = $(window).scrollTop();
  var viewportBottom = viewportTop + $(window).height();
  return elementBottom > viewportTop && elementTop < viewportBottom;
};

$(window).on('resize scroll', function() {
  scrollAnimations();
});

function scrollAnimations () {

  if ( $('.the-bench-chart').length ) {
    if( $('.the-bench-chart').isInViewport() && !$('.the-bench-chart').hasClass('active') ) {
      $('.the-bench-chart').addClass('active')
    }
  }
}

if ( $('.page-form').length ) {
  $('.page-form').each( (i, item) => {
    if ( $(item).data('header') ) {
      console.log($(item).data('header'))
      $('h4', item).html($(item).data('header'))
    }
    if ( $(item).data('submit') ) {
      console.log($(item).data('submit'))
      $('input[type="submit"]', item).val($(item).data('submit'))
    }
  })
}

$(window).scroll(function(){

    //Navbar color change
    if ($(window).scrollTop() > 100){
        $('.site-header').addClass('white');
    }
    else {
      $('.site-header').removeClass('white');
    }

    if ( $('.main-navigation').length ) {
      var fixed = $(".main-navigation");
      var fixed_position = $(".main-navigation").offset().top;
      var fixed_height = $(".main-navigation").height();

      var addClass = false;
      $('footer').each(function(){

          var toCross_position = $(this).offset().top;
          var toCross_height = $(this).height();

          if (fixed_position + fixed_height  < toCross_position) {
              //fixed.removeClass('white');
          } else if (fixed_position > toCross_position + toCross_height) {
              //fixed.removeClass('white');
          } else {
              addClass = true;
          }
      });
      if(addClass == true){
          fixed.addClass('light');
      }else{
          fixed.removeClass('light');
      }
    }

		//var fixed_position = $(".fixed").offset().top;
    //var fixed_height = $(".fixed").height();


	});

  $('document').ready(function(){
    if( $(".content").hasClass( "archived" ) ) {
      $('body').addClass("archived");
    }
  });
