<?php
function get_how_we_work_section($section) {
  ob_start(); ?>
  <div class="content">
    <h2><?php echo $section['title']; ?></h2>
    <?php if ( isset($section['steps']) ) { ?>
      <div class="bench-steps">
        <?php
        foreach( $section['steps'] as $key=>$step ) { ?>
          <div class="step row">
            <div class="step-num"><?php echo $key + 1; ?></div>
            <div class="col-6 illustration-container">
              <?php
              if ( $svg = $step['svg_selection'] ) {
                echo get_svg_by_name($svg);
              } ?>
            </div>
            <div class="col-6">
              <?php
              if ( isset($step['title']) ) { ?>
                <h6><?php echo $step['title']; ?></h6>
              <?php
              }
              if ( isset($step['subtitle']) ) { ?>
                <h3><?php echo $step['subtitle']; ?></h3>
              <?php
              }
              if ( $svg = $step['svg_selection'] ) { ?>
                <div class="illustration-container mobile">
                  <?php
                  if ( $svg = $step['svg_selection'] ) {
                    echo get_svg_by_name($svg);
                  } ?>
                </div>
              <?php
              }
              if ( isset($step['description']) ) { ?>
                <?php echo $step['description']; ?>
              <?php
              } ?>
            </div>
          </div>
        <?php
        } ?>
      </div>
    <?php
    } ?>
  </div>
  <?php
  return ob_get_clean();
}

function get_why_the_bench_section($section) {
  ob_start(); ?>
    <div class="content">
      <h2><?php echo $section['title']; ?></h2>
      <div class="row">
        <div class="col-6">
          <?php echo $section['description']; ?>
        </div>
        <div class="col-6">
          <?php echo $section['additional_content']; ?>
        </div>
      </div>
    </div>
  <?php
  return ob_get_clean();
}

function get_the_opportunity_briefs_section($section) {
  ob_start(); ?>
  <div class="content">
    <h3><?php echo $section['title']; ?></h3>
    <div class="row">
      <?php
      if ( isset($section['opportunities']) ) { ?>
        <div class="col-5">
          <div class="opportunities">

          </div>
        </div>
        <div class="col-7">
          <div class="opportunity-slide-bg">
            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
              	 viewBox="0 0 497 443" style="enable-background:new 0 0 497 443;" xml:space="preserve">
              <style type="text/css">
              	.st0{opacity:0.1;fill:#97D1CD;enable-background:new    ;}
              </style>
              <g id="Design">
              	<g id="Candidate_Desktop" transform="translate(-845.000000, -180.000000)">
              		<g id="Group-18" transform="translate(845.000000, 180.000000)">
              			<path id="Fill-1" class="st0" d="M21.3,394.4c13.9,14.9,29.2,17.2,106.1,26.8c143.5,17.8,215.6,26.5,233.9,19.3
              				c79.9-31.6,152.8-137.4,132.3-250.9C477.7,102.4,409.6,26.3,322.9,8.7C191.4-17.9,97.3,106.2,73.9,137
              				C15.1,214.5-27.6,342,21.3,394.4"/>
              		</g>
              	</g>
              </g>
              </svg>

          </div>
          <div id="opportunities-slider">
            <?php
            foreach( $section['opportunities'] as $key=>$opportunity ) { ?>
              <div class="opportunity-slide" data-title="<?php echo $opportunity['title']; ?>" data-color="<?php echo $opportunity['image_background_color']; ?>">
                <div class="slide-content">
                  <div class="image-container">
                    <div class="image" style="background-image: url('<?php echo $opportunity['image']['sizes']['large']; ?>')">
                    </div>
                  </div>
                  <?php
                  if ( $description = $opportunity['description'] ) { ?>
                    <div class="opportunity-slide-desc">
                      <?php echo $description; ?>
                    </div>
                  <?php
                  } ?>
                </div>
              </div>
            <?php
            } ?>
          </div>
        </div>
      <?php
      } ?>
    </div>
  </div>
  <?php
  return ob_get_clean();
}

function get_news_section($section) {
  ob_start(); ?>
  <div class="content">
    <div class="grey-line"></div>
    <h2><?php echo $section['title']; ?></h2>
    <div id="news-section-wrapper">
      <div id="news-section-content">
        <div class="news-item external-news">
          <div class="external-news-content">
            <h3>Latest News</h3>
            <div class="external-news-links">
              <?php
              $args = [
                'post_type' => 'external-post',
                'posts_per_page' => 10
              ];
              $ext_news_query = new WP_Query( $args );
              if( $ext_news_query->have_posts() ):
                while ( $ext_news_query->have_posts() ) : $ext_news_query->the_post(); ?>
                  <a class="external-news-link" target="_blank" href="<?php echo get_field('external_post_url'); ?>">
                    <?php the_title(); ?>
                  </a>
                <?php
                endwhile; wp_reset_query();
              endif; ?>
            </div>
          </div>
        </div>
        <?php
        $args = [
          'post_type' => 'post',
          'posts_per_page' => 3
        ];
        $news_query = new WP_Query( $args );
        if( $news_query->have_posts() ):
          while ( $news_query->have_posts() ) : $news_query->the_post();
            $news_image = get_the_post_thumbnail_url(get_the_ID(), 'medium'); ?>
            <div class="news-item internal-news">
              <div class="news-item-content-wrapper">
                <a class="news-item-content bg-centered" href="<?php the_permalink(); ?>" style="background-image:url(<?php echo $news_image; ?>);">
                  <h3 class="news-title">
                    <?php the_title(); ?>
                  </h3>
                </a>
              </div>
            </div>
          <?php
          endwhile; wp_reset_query();
        endif; ?>
      </div>
    </div>
  </div>
  <?php
  return ob_get_clean();
}

function get_divider_section($section) {
  ob_start(); ?>
    <div class="divider" style="background-image:url(<?php echo $section['image']['sizes']['large']; ?>);"></div>
  <?php
  return ob_get_clean();
}
